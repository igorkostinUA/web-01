const
    { pathes } = require('./package.json'),
    gulp = require('gulp'),
    { series, parallel } = require('gulp'),
    pug = require('gulp-pug'),
    sass = require('gulp-sass'),
    plumber = require('gulp-plumber')

function compilePug(cb) {
    return gulp.src(`${pathes.src}/${pathes.pug}`)
        .pipe(pug({ pretty: true }))
        .pipe(gulp.dest(pathes.dev))

    cb()
}

function compileScss(cb) {
    return gulp.src(`${pathes.src}/${pathes.scss}`)
        .pipe(plumber())
        .pipe(sass({ outputStyle: 'expanded' }))
        .pipe(gulp.dest(pathes.dev))

    cb()
}

function compileJS(cb) {
    return gulp.src(`${pathes.src}/${pathes.js}`)
        .pipe(gulp.dest(`${pathes.dev}`))

    cb()
}

function moveFonts(cb) {
    return gulp.src(`${pathes.assets}/${pathes.fonts}/${pathes.all}`)
        .pipe(gulp.dest(`${pathes.dev}/${pathes.fonts}`))

    cb()
}

function moveImages(cb) {
    return gulp.src(`${pathes.assets}/${pathes.images}/${pathes.all}`)
        .pipe(gulp.dest(`${pathes.dev}/${pathes.images}`))

    cb()
}

function moveSVG(cb) {
    return gulp.src(`${pathes.assets}/${pathes.svg}/${pathes.all}`)
        .pipe(gulp.dest(`${pathes.dev}/${pathes.svg}`))

    cb()
}

function watchFiles(cb) {
    gulp.watch(`${pathes.src}/${pathes.pug}`, compilePug)
    gulp.watch(`${pathes.src}/${pathes.scss}`, compileScss)
    gulp.watch(`${pathes.src}/${pathes.js}`, compileJS)
    gulp.watch(`${pathes.assets}/${pathes.fonts}/${pathes.all}`, moveFonts)
    gulp.watch(`${pathes.assets}/${pathes.images}/${pathes.all}`, moveImages)
    gulp.watch(`${pathes.assets}/${pathes.svg}/${pathes.all}`, moveSVG)
    
    cb()
}

const build = series(moveFonts, moveSVG, moveImages, parallel(compilePug, compileScss, compileJS, watchFiles))

exports.moveSVG = moveSVG
exports.compileJS = compileJS
exports.compileFonts = moveFonts
exports.compileImages = moveImages
exports.compilePug = compilePug
exports.compileScss = compileScss
exports.watchFiles = watchFiles
exports.default = build